#ifndef MODEL
#define MODEL

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>


//Define parameters and variables

#define GRID_SIZE 100
#define IMMUNITY_DURATION 12
#define CHANCE_REPRODUCE 1
#define LIFESPAN 4160

//Global Variables
int infected_count;
int immune_count;
int death_count;
int count_agents ;
int total_agents ;
double infected; //%
double immune; //%
double death;//%



//Define Structs

typedef struct agent
{
    bool sick;
    int remaining_immunity;
    int sick_time;
    int age;
    bool valid;
} agent;


typedef struct channels{
  pthread_mutex_t* lock;
  agent* ag;
  int i;
  int j;
  int duration;
  int carrying_capacity;
  int chance_recover;
  int infectiousness;
  int death_chance;
}channel;

//Define functions

agent agent_matrix[GRID_SIZE][GRID_SIZE]; //parallel model
agent agent_matrixS[GRID_SIZE][GRID_SIZE]; //sequential model

//Sequential Model

//Function to setup global variables and initiate model 
void setup(int weeks, int initial_agents, int carrying_capacity, int infectiousness, int chance_recover, int death_chance, int duration);

//Function to create agents, with initial number of 10 people infected
void create_agents(int initial_agents);

//Function to indicate agent is sick 
void get_sick(agent* agent);

//Function to indicate agents is healthy 
void get_healthy(agent* agent);

//Function to indicate agent is immune 
void become_immune(agent* agent);

//Function to initiate model. Go through the matrix and call on move, get_older, recover or die, infect and reproduce functions. 
//Calls update global variables function at the end to update global variables 
void go(int duration, int carrying_capacity, int chance_recover, int infectiousness, int death_chance);

//Count alive agents, sick agents and immune agents 
void update_global_variables(agent* ag);

//Increase age of agent
void get_older(agent* agent);

//Move agent to a new random position 
void move(agent* agent);

//Check surrounding positions to infect agents nearby depending on infectiousness
void infect(agent* agent, int i, int j, int infectiousness);

//Set agent to recover or die depending on chance of recovery and chance of death 
void recover_or_die(agent* agent,int duration, int chance_recover, int death_chance);

//Increase number of agents and assign position in matrix if carrying capacity has not been reached
void reproduce(agent* agent, int carrying_capacity);

//Assign immunity to agent
bool report_immune(agent* agent);

//Initialize matrix 
void ini_matrix();

//Print function for debugging
void printMatrix(agent agent_matrixS[GRID_SIZE][GRID_SIZE]);

//Test function for debugging
void test(int i,int j);

//------------------------------//

//Parallel Model

//Function to setup global variables and initiate model 
void setupP(int weeks, int initial_agents, int carrying_capacity, int infectiousness, int chance_recover, int death_chance, int duration);

//Function to create agents, with initial number of 10 people infected
void create_agentsP(int initial_agents);

//Print function for debugging
void printTable(agent ag[][100]);

//Function to indicate agent is sick 
void get_sickP(agent* agent, pthread_mutex_t* ulock);

//Function to indicate agents is healthy 
void get_healthyP(agent* agent);

//Function to indicate agent is immune 
void become_immuneP(agent* agent, pthread_mutex_t* ulock);

//Function to run model calling functions to move, reproduce, get older, recover or die, infect and update global variables for each thread
void goP(agent *ag, int i, int j, pthread_mutex_t* ulock, int duration, int carrying_capacity, int chance_recover, int infectiousness, int death_chance);

//Function to obtain parameters from channel struct to call go function
void *go_th(void *arguments);

//Count alive agents, sick agents and immune agents 
void update_global_variablesP(agent* ag, pthread_mutex_t* ulock);

//Increase age of agent
void get_olderP(agent* agent, pthread_mutex_t* ulock);

//Move agent to a new random position 
void moveP(agent* agent, pthread_mutex_t* ulock);

//Check surrounding positions to infect agents nearby depending on infectiousness
void infectP(agent* agent, int i, int j, pthread_mutex_t* ulock, int infectiousness);

//Set agent to recover or die depending on chance of recovery and chance of death 
void recover_or_dieP(agent* agent, pthread_mutex_t* ulock, int duration, int chance_recover, int death_chance);

//Increase number of agents and assign position in matrix if carrying capacity has not been reached
void reproduceP(agent* agent, pthread_mutex_t* ulock, int carrying_capacity);

//Assign immunity to agent
bool report_immuneP(agent* agent);

//Initialize matrix 
void ini_matrixP();


#endif
