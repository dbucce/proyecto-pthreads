#include "../include/model.h"




void setup(int weeks, int initial_agents, int carrying_capacity, int infectiousness, int chance_recover, int death_chance, int duration)
{

  //initialize variables and matrix 
  ini_matrix();
  infected_count = 0;
  immune_count = 0;
  count_agents = initial_agents;

  //create agents 
  create_agents(initial_agents);

  for (int i = 0; i < weeks; i++)
  {
    //save weeks on results file 
    FILE *fp_weeks;
    fp_weeks = fopen("src/weeks.txt", "a");
    fprintf(fp_weeks, "%i\n", i);
    fclose(fp_weeks);

    //start to run the model 
    go( duration,  carrying_capacity,  chance_recover, infectiousness, death_chance);

    //Calculate percentages 
    infected = ((float)infected_count/(float)count_agents) * 100.0;
    immune = ((float)immune_count/(float)count_agents) * 100.0;

    //Save data on result files 
    FILE *fp_people;
    fp_people = fopen("src/people.txt", "a");
    fprintf(fp_people, "%i\n", count_agents);
    fclose(fp_people);
    FILE *fp_immune;
    fp_immune = fopen("src/immune.txt", "a");
    fprintf(fp_immune, "%i\n", immune_count);
    fclose(fp_immune);
    FILE *fp_sick;
    fp_sick = fopen("src/sick.txt", "a");
    fprintf(fp_sick, "%i\n", infected_count);
    fclose(fp_sick);
    FILE *fp_healthy;
    fp_healthy = fopen("src/healthy.txt", "a");
   // printf("infected: %d  agents: %d\n", infected_count, count_agents);
    fprintf(fp_healthy, "%i\n", count_agents-infected_count);
    fclose(fp_healthy);
   // printf("Infected: %f\n", infected);
   // printf("Immune: %f\n", immune);
    if (count_agents <= 0){
      break;
    }
  }
}


void create_agents(int initial_agents)
{

  int count = 0;
  bool array_temp[initial_agents];
  //// printf("Initial agents: %i\n", initial_agents);
  for (int i = 0; i < initial_agents; i++)
  {
    // 10 agents are infected
    if (i < 10)
    {
      array_temp[i] = true;
      infected_count++;
    }
    else
    {
      array_temp[i] = false;
    }
  }

 // Agents are places randomly in the matrix
  while (count < initial_agents)
  {
    int i = rand() % GRID_SIZE;
    int j = rand() % GRID_SIZE;
    while(agent_matrixS[i][j].valid)
    {
      i = rand() % GRID_SIZE;
      j = rand() % GRID_SIZE;
    }
    //Set up matrix variables 
    agent_matrixS[i][j].remaining_immunity = 0;
    agent_matrixS[i][j].sick_time = 0;
    agent_matrixS[i][j].age = (rand() % LIFESPAN) + 1;

    agent_matrixS[i][j].sick = array_temp[count];

    agent_matrixS[i][j].valid = true;
    count++;

  }

}

void get_sick(agent* agent)
{
//update sick parameter 
  agent->sick = true;
  agent->remaining_immunity = 0;
}

void get_healthy(agent* agent)
{
  //updte sick parameter 
  agent->sick = false;
  agent->remaining_immunity = 0;
  agent->sick_time = 0;
}

void become_immune(agent* agent)
{
  //update sicl and immunity parameters 
  agent->sick = false;
  agent->remaining_immunity = IMMUNITY_DURATION;
  agent->sick_time = 0;
}



void go(int duration, int carrying_capacity, int chance_recover, int infectiousness, int death_chance)
{
  for(int i = 0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      if(agent_matrixS[i][j].valid){
        // get older
        get_older(&agent_matrixS[i][j]);
      }
    }
  }
  // move
  for(int i = 0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      if(agent_matrixS[i][j].valid){
        move(&agent_matrixS[i][j]);
      }
    }
  }

  for(int i = 0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      if(agent_matrixS[i][j].valid){
        // if sick recover or die
        if (agent_matrixS[i][j].sick)
        {
          recover_or_die(&agent_matrixS[i][j], duration, chance_recover, death_chance);
        }
        if (agent_matrixS[i][j].sick && agent_matrixS[i][j].valid)
        {
          infect(&agent_matrixS[i][j],i,j, infectiousness);
        }
        // if it didnt die, reproduce
        if (agent_matrixS[i][j].valid)
        {
          reproduce(&agent_matrixS[i][j], carrying_capacity);
        }
      }

    }
  }
  //restart counters 
  infected_count = 0;
  immune_count = 0;
  for(int i = 0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      update_global_variables(&agent_matrixS[i][j]);
    }
  }


}

void update_global_variables(agent* ag){
//If all agents are dead, stop de program 
  if (count_agents<=0){
    printf("All agents are dead.");
    exit(1);
  }
  //count sick and immune people
  if(ag->valid == 1){
    if(ag->sick){
      infected_count++;
    }
    if(report_immune(ag) ){
      immune_count++;
    }
  }
}

void get_older(agent* agent)
{
//increase age 
  agent->age++;

  if(agent->age > LIFESPAN)
  {
    if (count_agents > 0){
      count_agents--;
    }    
    agent->valid = false;
    

  }
  // Update remaining immunity
  else if( report_immune(agent) )
  {
    if (agent->remaining_immunity != 0)
    {
      agent->remaining_immunity--;
    }
  }
  // Update sick time
  else if(agent->sick)
  {
    agent->sick_time++;
  }
}

void move(agent* agent)
{
  int i = rand() % GRID_SIZE;
  int j = rand() % GRID_SIZE;
  // Find empty position
  while(agent_matrixS[i][j].valid)
  {
    i = rand() % GRID_SIZE;
    j = rand() % GRID_SIZE;
  }
  // Move to new position
  agent_matrixS[i][j].valid = true;
  agent_matrixS[i][j].sick = agent->sick;
  agent_matrixS[i][j].sick_time = agent->sick_time;
  agent_matrixS[i][j].age = agent->age;
  agent_matrixS[i][j].remaining_immunity = agent->remaining_immunity;
  // Remove from old position
  agent->valid = false;
}

void infect(agent* agent, int i, int j, int infectiousness)
{
  if (agent->sick && agent->sick_time != 0)
  {
    // Check surrounding positions
    for(int x = 0; x < 2; x++)
    {
      for(int y = 0; y < 2; y++)
      {
        // + X + Y
        // Last row
        if(i == GRID_SIZE-1 && x == 1)
        {
        }
        // last column
        else if (j == GRID_SIZE-1 && y == 1)
        {
        }
        else if (agent_matrixS[i+x][j+y].valid && agent_matrixS[i+x][j+y].sick == false && report_immune(&agent_matrixS[i+x][j+y]) == false && rand() % 100 < infectiousness)
        {
          get_sick(&agent_matrixS[i+x][j+y]);

        }
        // - X - Y
        // first row
        if(i == 0 && x == 1)
        {
        }
        // first column
        else if (j == 0 && y == 1)
        {
        }
        else if (agent_matrixS[i-x][j+y].valid && agent_matrixS[i-x][j+y].sick == false && report_immune(&agent_matrixS[i-x][j+y]) == false && rand() % 100 < infectiousness)
        {
          get_sick(&agent_matrixS[i-x][j+y]);

        }
        // + X - Y
        // Last row
        if(i == GRID_SIZE-1 && x == 1)
        {
        }
        // first column
        else if (j == 0 && y == 1)
        {
        }
        else if (agent_matrixS[i+x][j-y].valid && agent_matrixS[i+x][j-y].sick == false && report_immune(&agent_matrixS[i+x][j-y]) == false && rand() % 100 < infectiousness)
        {
          get_sick(&agent_matrixS[i+x][j-y]);

        }
        // - X + Y
        // first row
        if(i == 0 && x == 1)
        {
        }
        // last column
        else if (j == GRID_SIZE-1 && y == 1)
        {
        }
        else if (agent_matrixS[i-x][j+y].valid && agent_matrixS[i-x][j+y].sick == false && report_immune(&agent_matrixS[i-x][j+y]) == false && rand() % 100 < infectiousness)
        {
          get_sick(&agent_matrixS[i-x][j+y]);

        }
      }
    }
  }
}

void recover_or_die(agent* agent, int duration,int chance_recover, int death_chance)
{
  if (agent->sick_time > duration)
  {
    // in case of recovery
    if (rand() % 100 < chance_recover)
    {
      become_immune(agent);
    }
    // in case of death
    else if (rand() % 100 < death_chance)
    {
      //printf("Death\n");
      agent->valid = false;
      if (count_agents > 0){
        count_agents--;
      }  
    }
    else
    {
      get_healthy(agent);
    }
  }
}

void reproduce(agent* agent, int carrying_capacity)
{
  if (count_agents < carrying_capacity && rand() % 100 <= CHANCE_REPRODUCE)
  {
    //printf("reproduce\n");
    count_agents++;
    int i = rand() % GRID_SIZE;
    int j = rand() % GRID_SIZE;
    // find empty position
    while(agent_matrixS[i][j].valid)
    {
      i = rand() % GRID_SIZE;
      j = rand() % GRID_SIZE;
    }
    // place new agent
    agent_matrixS[i][j].age = 1;
    agent_matrixS[i][j].valid = true;
    get_healthy(&agent_matrixS[i][j]);
  }
}

//return true if immune 
bool report_immune(agent* agent)
{
  if (agent->remaining_immunity > 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//set matrix parameters to 0
void ini_matrix()
{
  for(int i=0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      agent_matrixS[i][j].valid = 0;
      agent_matrixS[i][j].sick = false;
      agent_matrixS[i][j].remaining_immunity = 0;
      agent_matrixS[i][j].sick_time = 0;
      agent_matrixS[i][j].age = 0;
    }
  }
}

//print for debugging 
void printMatrix(agent agent_matrixS[GRID_SIZE][GRID_SIZE])
{
  for(int i=0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      // printf("%s%d%s%d%s","i: ",i,"j: ",j,"  ");
      if (agent_matrixS[i][j].valid == false)
      {
        printf("x ");
      }
      else if(agent_matrixS[i][j].sick)
      {
        printf("e ");
      }
      else if(agent_matrixS[i][j].remaining_immunity > 0)
      {
        printf("i ");
      }
      else
      {
        printf("s ");
      }
    }
    printf("\n");
  }
}

//test for debugging 
void test(int i,int j)
{
  // Check surrounding positions
  for(int x = 0; x < 2; x++)
  {
    for(int y = 0; y < 2; y++)
    {

      // Last row
      if(i == GRID_SIZE-1 && x == 1)
      {
        printf("last row\n" );
        printf("%s%d%s%d \n","i: ",i+x ,"j: ",j+y);
      }
      // last column
      else if (j == GRID_SIZE-1 && y == 1)
      {
        printf("last column\n" );
        printf("%s%d%s%d \n","i: ",i+x ,"j: ",j+y);
      }
      else
      {
        get_sick(&agent_matrixS[i+x][j+y]);
        printf("%s%d%s%d \n","i: ",i+x,"j: ",j+y);
      }
      // first row
      if(i == 0 && x == 1)
      {
        printf("first row\n" );
        printf("%s%d%s%d \n","i: ",i-x ,"j: ",j-y);;
      }
      // first column
      else if (j == 0 && y == 1)
      {
        printf("first column\n" );
        printf("%s%d%s%d \n","i: ",i-x ,"j: ",j-y);
      }
      else
      {
        get_sick(&agent_matrixS[i-x][j-y]);
        printf("%s%d%s%d \n","i: ",i-x ,"j: ",j-y);
      }
    }
  }
}
