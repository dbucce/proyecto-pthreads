#ifndef MAIN
#define MAIN

#include <stdlib.h>
#include <stdio.h>
#include "../include/model.h"


int main(int argc, char * argv []){
    if (argc<8){
        printf("Error. Provide necessary parameters.\n");
        return -1; 
    }
    int weeks = atoi(argv[1]);
    int initial_agents = atoi(argv[2]);
    int carrying_capacity = atoi(argv[3]);
    int infectiousness = atoi(argv[4]);
    int chance_recover = atoi(argv[5]);
    int death_chance = atoi(argv[6]);
    int duration = atoi(argv[7]);



    
    setup(weeks, initial_agents, carrying_capacity, infectiousness, chance_recover, death_chance, duration); 
    printf("Modelo secuencial terminado.\n");

    setupP(weeks, initial_agents, carrying_capacity, infectiousness, chance_recover, death_chance, duration); 
    printf("Modelo paralelo terminado.\n");

    return 0; 
}


#endif
