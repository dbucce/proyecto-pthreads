#include "../include/model.h"



int counterM = 0;
void setupP(int weeks, int initial_agents, int carrying_capacity, int infectiousness, int chance_recover, int death_chance, int duration){  

  //initialize matrix and create agents 
  ini_matrixP();
  create_agentsP(initial_agents);

  //create lock
  pthread_mutex_t lock;
  channel pack;
  agent local_matrix[GRID_SIZE][GRID_SIZE];
  int result;

  //set up struct parameters and global constants 
  count_agents = initial_agents;
  total_agents = initial_agents;
  pack.carrying_capacity = carrying_capacity;
  pack.death_chance = death_chance;
  pack.chance_recover = chance_recover;
  pack.duration = duration;
  pack.infectiousness = infectiousness;

  for (int k = 0; k < weeks; k++){
    //save weeks on results file 
    FILE *fp_weeks; 
    fp_weeks = fopen("src/weeksP.txt", "a");
    fprintf(fp_weeks, "%i\n", k);
    fclose(fp_weeks);
    pthread_mutex_init(&lock, NULL); 
    // checks amount of agents
    int count = 0; 

    //set values on local matrix 
    for (size_t u = 0; u < GRID_SIZE; u++){
      for (size_t v = 0; v < GRID_SIZE; v++){
        local_matrix[u][v].age = agent_matrix[u][v].age;
        local_matrix[u][v].remaining_immunity = agent_matrix[u][v].remaining_immunity;
        local_matrix[u][v].sick = agent_matrix[u][v].sick;
        local_matrix[u][v].sick_time = agent_matrix[u][v].sick_time;
        local_matrix[u][v].valid = agent_matrix[u][v].valid;
        if (agent_matrix[u][v].valid){
          count++;
        }        
      }      
    }
    
    //set up threads 
    pthread_t threads[count/*GRID_SIZE*GRID_SIZE*/];
   
    infected = 0;
    immune = 0;
    infected_count = 0;
    immune_count = 0;    

    counterM = 0;
    int numb = 0; 

    //create threads with channel parameters    
    for(int i = 0; i < GRID_SIZE; i++){
      for(int j = 0; j < GRID_SIZE; j++){
        if(local_matrix[i][j].valid){
        
          pack.ag = &(agent_matrix[i][j]);
       
          pack.i = i;
          pack.j = j;          
          pack.lock = &lock;
     
          result = pthread_create(&threads[numb/*(i*GRID_SIZE)+j*/], NULL, go_th, &pack);
          assert(!result); 
          numb++;         
        }
      }
    }
 
    //join threads 
    for (int x = 0; x < numb; x++){
      result = pthread_join(threads[x], NULL);
      assert(!result);
    }

    // get percentages   
    infected = ((float)infected_count/(float)count_agents) * 100.0;
    immune = ((float)immune_count/(float)count_agents) * 100.0;
    death = ((float)death_count/(float)total_agents) * 100.0;
  
   // printf("Total Agents: %d  Alive Agents: %d  Infected Agents: %d  Dead Agents: %d  Inmune Agents: %d  Infected Percentage: %f  Death Percentage: %f  Inmune Percentage: %f\n",total_agents, (total_agents-death_count), infected_count, death_count, immune_count,infected, death, immune);

    //free lock                      
    pthread_mutex_destroy(&lock);

    //Save data on result files 
    FILE *fp_people; 
    fp_people = fopen("src/peopleP.txt", "a");
    fprintf(fp_people, "%i\n", total_agents-death_count);
    fclose(fp_people);
    FILE *fp_immune; 
    fp_immune = fopen("src/immuneP.txt", "a");
    fprintf(fp_immune, "%i\n", immune_count);
    fclose(fp_immune);
    FILE *fp_sick; 
    fp_sick = fopen("src/sickP.txt", "a");
    fprintf(fp_sick, "%i\n", infected_count);
    fclose(fp_sick);
    FILE *fp_healthy; 
    fp_healthy = fopen("src/healthyP.txt", "a");
    fprintf(fp_healthy, "%i\n", total_agents-infected_count-death_count);
    fclose(fp_healthy);
    if (count_agents <= 0){
      printf("All agents are dead.\n");
      break;
    }
      
  }
  
}

//Print for debugging
void printTable(agent ag[][100]){
  for (int i = 0; i < 100; i++){
    for (int j = 0; j < 100; j++){      
      if (ag[i][j].valid){
        if (ag[i][j].sick){
          printf("2 ");
        }else if (ag[i][j].remaining_immunity>0){
          printf("3 ");
        }else{
          printf("1 ");
        }        
      }else{
        printf("0 ");
      }      
    }
    printf("\n");    
  }  
}

void *go_th(void *arguments){
  //set up channel struct 
  channel* chan = (channel*)arguments;
  //// printf("Valid: %d\n",(chan->ag)->valid);
  if ((chan->ag)->valid == 1){
    counterM++;
    //// printf("Deatch chance: %d\n",chan->death_chance);
    goP(chan->ag, chan->i, chan->j, chan->lock, chan->duration, chan->carrying_capacity, chan->chance_recover, chan->infectiousness, chan->death_chance);
  }  
}

void create_agentsP(int initial_agents)
{
  int count = 0;
  bool array_temp[initial_agents];
  for (int i = 0; i < initial_agents; i++)
  {
    // 10 agents are infected
    if (i < 10)
    {
      array_temp[i] = true;
    }
    else
    {
      array_temp[i] = false;
    }
  }

 // Agents are placed randomly in the matrix
  while (count < initial_agents)
  {
    int i = rand() % GRID_SIZE;
    int j = rand() % GRID_SIZE;
    while(agent_matrix[i][j].valid)
    {
      i = rand() % GRID_SIZE;
      j = rand() % GRID_SIZE;
    }
    agent_matrix[i][j].remaining_immunity = 0;
    agent_matrix[i][j].sick_time = 0;
    agent_matrix[i][j].age = rand() % LIFESPAN + 1;
    agent_matrix[i][j].sick = array_temp[count];
    agent_matrix[i][j].valid = true;
    count++;
  }

}

//update sick parameter 
void get_sickP(agent* agent, pthread_mutex_t* ulock)
{
  pthread_mutex_lock(ulock);
  agent->sick = true;
  agent->remaining_immunity = 0;
  pthread_mutex_unlock(ulock);
}

//update sick parameter 
void get_healthyP(agent* agent)
{
  // pthread_mutex_lock(ulock);
  agent->sick = false;
  agent->remaining_immunity = 0;
  agent->sick_time = 0;
  // pthread_mutex_unlock(ulock);
}

//update sick and immunity parameters 
void become_immuneP(agent* agent, pthread_mutex_t* ulock)
{
  pthread_mutex_lock(ulock);
  agent->sick = false;
  agent->remaining_immunity = IMMUNITY_DURATION;
  agent->sick_time = 0;
  pthread_mutex_unlock(ulock);
}



void goP(agent *ag, int i, int j, pthread_mutex_t* ulock, int duration, int carrying_capacity, int chance_recover, int infectiousness, int death_chance){    
 
  update_global_variablesP(ag, ulock);//check
  // get older
  get_olderP(ag, ulock);  

  // if sick recover or die
  if (ag->sick){
    recover_or_dieP(ag, ulock, duration, chance_recover, death_chance);
    infectP(ag,i,j, ulock, infectiousness);
  }
     
  // if it didnt die, reproduce
  if (ag->valid){
    reproduceP(ag, ulock, carrying_capacity);
    // moveP
    moveP(ag, ulock);
  } 
  // pthread_mutex_unlock(ulock); 
}

void update_global_variablesP(agent* age, pthread_mutex_t* ulock){//paralelizacion jodida
  // Get amount of infected agents, immune agents and total agents
  //set lock to protect variables 
  pthread_mutex_lock(ulock);

  if(age->valid == 1){

    if(age->sick){
      infected_count++;
    }
    if(report_immuneP(age)){
   
      immune_count++;
    }

  }
  pthread_mutex_unlock(ulock); 
}

void get_olderP(agent* agent, pthread_mutex_t* ulock)
{
  agent->age++;

  // Die
  if(agent->age > LIFESPAN)
  {
    pthread_mutex_lock(ulock);
    agent->valid = false;
    if (count_agents > 0){
      count_agents--;
      death_count++;
    }    
    pthread_mutex_unlock(ulock);
  }
  // Update remianing immunity
  else if( report_immuneP(agent) )
  {
    if (agent->remaining_immunity != 0)
    {
      agent->remaining_immunity--;
    }
  }
  // Update sick time
  else if(agent->sick)
  {
    agent->sick_time++;
  }
}

void moveP(agent* agent, pthread_mutex_t* ulock)
{
  int i = rand() % GRID_SIZE;
  int j = rand() % GRID_SIZE;
  // Find empty position
  pthread_mutex_lock(ulock);
  while(agent_matrix[i][j].valid)
  {
    i = rand() % GRID_SIZE;
    j = rand() % GRID_SIZE;
  }
  // MoveP to new position
  agent_matrix[i][j].valid = true;
  agent_matrix[i][j].sick = agent->sick;
  agent_matrix[i][j].sick_time = agent->sick_time;
  agent_matrix[i][j].age = agent->age;
  agent_matrix[i][j].remaining_immunity = agent->remaining_immunity;
  // RemoveP from old position
  agent->valid = false;
  pthread_mutex_unlock(ulock);
}

void infectP(agent* agent, int i, int j, pthread_mutex_t* ulock, int infectiousness)
{
  if (agent->sick && agent->sick_time != 0)
  {
    // Check surrounding positions
    for(int x = 0; x < 2; x++)
    {
      for(int y = 0; y < 2; y++)
      {
        // + X + Y 
        // Last row
        if(i == GRID_SIZE -1 && x == 1)
        {
        }
        // last column
        else if (j == GRID_SIZE-1 && y == 1)
        {
        }
        else if (agent_matrix[i+x][j+y].valid && agent_matrix[i+x][j+y].sick == false && report_immuneP(&agent_matrix[i+x][j+y]) == false && rand() % 100 < infectiousness)
        {
          get_sickP(&agent_matrix[i+x][j+y], ulock);
        }
        // -X - Y 
        // first row
        if(i == 0 && x == 1)
        {
        }
        // first column
        else if (j == 0 && y == 1)
        {
        }
        else if (agent_matrix[i-x][j-y].valid && agent_matrix[i-x][j-y].sick == false && report_immuneP(&agent_matrix[i-x][j-y]) == false && rand() % 100 < infectiousness)
        {
          get_sickP(&agent_matrix[i-x][j-y], ulock);
        }
        //+ X - Y 
        //Last row 
        if(i == GRID_SIZE-1 && x == 1)
        {
        }
        // first column
        else if (j == 0 && y == 1)
        {
        }
        else if (agent_matrixS[i+x][j-y].valid && agent_matrixS[i+x][j-y].sick == false && report_immune(&agent_matrixS[i+x][j-y]) == false && rand() % 100 < infectiousness)
        {
          get_sickP(&agent_matrix[i+x][j-y], ulock);

        }
         // - X + Y
        // first row
        if(i == 0 && x == 1)
        {
        }
        // last column
        else if (j == GRID_SIZE-1 && y == 1)
        {
        }
        else if (agent_matrixS[i-x][j+y].valid && agent_matrixS[i-x][j+y].sick == false && report_immune(&agent_matrixS[i-x][j+y]) == false && rand() % 100 < infectiousness)
        {
          get_sickP(&agent_matrix[i-x][j+y], ulock);
        }
      }
    }
  }
}

void recover_or_dieP(agent* agent, pthread_mutex_t* ulock, int duration, int chance_recover, int death_chance)
{
  if (agent->sick_time > duration)
  {
    // in case of recovery
    if (rand() % GRID_SIZE <= chance_recover)
    {
      become_immuneP(agent, ulock);
    }
    // in case of death
    if (rand() % GRID_SIZE <= death_chance)
    {
      pthread_mutex_lock(ulock);
      agent->valid = false;
      if (count_agents > 0){
        count_agents--;
        death_count++;
      }      
      pthread_mutex_unlock(ulock);
    }
  }
}
int reproduceN = 0;
void reproduceP(agent* agent, pthread_mutex_t* ulock, int carrying_capacity)
{  
  if (count_agents < carrying_capacity && rand() % 100 <= CHANCE_REPRODUCE)
  {
    
    int i = rand() % GRID_SIZE;
    int j = rand() % GRID_SIZE;
    // find empty position
    pthread_mutex_lock(ulock);
    count_agents++;
    total_agents++; 
    reproduceN++;
    //// printf("Reproduce: %d  Current Agents: %d\n", reproduceN, count_agents);   
    while(agent_matrix[i][j].valid)
    {
      i = rand() % GRID_SIZE;
      j = rand() % GRID_SIZE;
    }
    // place new agent
    agent_matrix[i][j].age = 1;
    agent_matrix[i][j].valid = true;
    get_healthyP(&agent_matrix[i][j]);
    pthread_mutex_unlock(ulock);
  }
}

//return true if immune 
bool report_immuneP(agent* agent)
{
  if (agent->remaining_immunity > 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//initialize matrix parameters 
void ini_matrixP()
{
  for(int i=0; i < GRID_SIZE; i++)
  {
    for(int j = 0; j < GRID_SIZE; j++)
    {
      agent_matrix[i][j].valid = 0;
      agent_matrix[i][j].sick = false;
      agent_matrix[i][j].remaining_immunity = 0;
      agent_matrix[i][j].sick_time = 0;
      agent_matrix[i][j].age = 0;
    }
  }
}
