import sys
import plotly
import matplotlib.pyplot as pyplot 

#Obtain parameters 
def init(argv):
    fpeople = sys.argv[1]
    fweeks = sys.argv[2]
    fsick = sys.argv[3]
    fhealthy = sys.argv[4]
    fimmune = sys.argv[5]
    ftotal = sys.argv[6]
    
    return [fpeople, fweeks, fsick, fhealthy, fimmune, ftotal]


def main():
    data_files = init(sys.argv)
    
    people = int(data_files[0])
    
    #Read results files 
    with open(data_files[1], "r") as fweeks: 
        weeks = fweeks.readlines()
        weeks = [ float(i) for i in weeks ]

    with open(data_files[2], "r") as fsick: 
        sick = fsick.readlines()
        sick = [ float(i) for i in sick ]
    
    with open(data_files[3], "r") as fhealthy: 
        healthy = fhealthy.readlines()
        healthy = [ float(i) for i in healthy ]

    with open(data_files[4], "r") as fimmune: 
        immune = fimmune.readlines()
        immune = [ float(i) for i in immune ]

    with open(data_files[5], "r") as ftotal: 
        total = ftotal.readlines()
        total = [ float(i) for i in total ]
    

    #plot data 
    pyplot.plot(weeks, sick, label= 'sick')
    pyplot.legend(loc='sick')
    pyplot.plot(weeks, healthy, label = 'healthy')
    pyplot.legend(loc='healthy')
    pyplot.plot(weeks, immune, label ='immune')
    pyplot.legend(loc='immune')
    pyplot.plot(weeks, total, label ='total')
    pyplot.legend(loc='total')
    pyplot.xlim(0,weeks[len(weeks)-2])
    pyplot.ylim(0,max(total))
    pyplot.suptitle('Populations')
    pyplot.xlabel('Weeks')
    pyplot.ylabel('People')
    pyplot.grid(which='both', axis='both', color="#DDDDDD")
    pyplot.show()



#####################
main()
