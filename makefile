
#######################################
# Variables
#######################################

# C compiler
CC=gcc

# Files
BASEDIR=.
INCLDIR=$(BASEDIR)/include
SRCDIR=$(BASEDIR)/src
BINDIR=$(BASEDIR)/bin

OUTFILE=model
TXTFILES = $(SRCDIR)/healthy.txt $(SRCDIR)/immune.txt $(SRCDIR)/people.txt $(SRCDIR)/sick.txt $(SRCDIR)/weeks.txt $(SRCDIR)/healthyP.txt $(SRCDIR)/immuneP.txt $(SRCDIR)/peopleP.txt $(SRCDIR)/sickP.txt $(SRCDIR)/weeksP.txt
# Compiler options and flags
CFLAGS= -lpthread -I $(INCLDIR) -o $(BINDIR)/$(OUTFILE)



#######################################
# Targets
#######################################

all: clean build run clean

build:
	@echo "Compiling:"
	@$(CC) $(SRCDIR)/*.c $(CFLAGS) 
	@echo "Done."
clean:
	@rm -rf $(BINDIR)/*
	@rm -f *~
	@rm -rf .vscode
	@rm -rf $(TXTFILES)
	@echo "Files cleaned"

run:
	@$(BINDIR)/$(OUTFILE) 50 200 400 70 10 5 4


.PHONY: all clean doc
