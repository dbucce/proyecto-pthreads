Proyecto 2: PThreads
El programa consiste en un modelo del comportamiento de un virus bajo ciertas condiciones para una cantidad especifica de personas. El modelo se paraleliza para mejorar el rendimiento. Además, se incluye un script en python para general los gráficos del modelo. Al ejecutar el pograma se crean archivos de texto con los datos generados de personas enfermas, saludables, total de personas e inmunes, los cuales se pasan como parámetros a un programa de python para poder graficar los datos.

Daniela Bucce B61256
Alberto Hernandez B63348
Natalia Bolaños B61127

Para el modelo: 

Para compilar utilizar:
    make build

Para correr el programa con los siguientes parámetros como ejemplo: 50 200 400 70 10 5 4 
    make run

Para correr el programa con otros parámetros deseados: 
    ./bin/model a b c d e f g
    donde:
    a: cantidad total de semanas 
    b: cantidad de personas iniciales
    c: capacidad total de personas
    d: infecciosidad (0-100)
    e: chance de recuperación (0-100)
    f: chance de muerte (0-100)
    g: duración del virus en semanas

Para limpiar los archivos de texto (*IMPORTANTE: es necesario borrar los archivos antes de correr el programa para tener los datos actualizados y correctos):
    make clean

Se puede compilar, correr el ejemplo y luego borrar con:
    make

**En caso de que todos los agentes mueran durante las semanas de prueba, se termina el programa indicando un error y los archivos de texto llegan hasta la última semana donde habían agentes vivos. 
____________________________________________
Para el script de python: 

Es necesesario tener instalado las siguientes bibliotecas de python: 
    plotly
    matplot.lib

Para ejecutar el programa desde el directorio de src: 

Modelo secuencial:

python3 plotter.py n weeks.txt sick.txt healthy.txt immune.txt people.txt

Modelo paralelo: 

python3 plotter.py n weeksP.txt sickP.txt healthyP.txt immuneP.txt peopleP.txt

donde n es la cantidad de personas iniciales con la que se corrió el modelo. 